#include <iostream>

#include<TimeA.h>
#include<ZuluTime.h>
#include<CivilianTime.h>
#include<BrazilianTime.h>

using namespace std;

int main()
{

    TimeA *times[4];

    times[0] = new TimeA(14, 30);
    times[1] = new CivilianTime(2, 30, 1);
    times[2] = new ZuluTime(14, 30, 6);
    times[3] = new BrazilianTime(16, 35, 10);

    for(int i = 0; i< 4;i++)
        times[i]->tell();

    return 0;
}
