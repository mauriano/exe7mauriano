#include "CivilianTimeImp.h"

CivilianTimeImp::CivilianTimeImp()
{
    //ctor
}

CivilianTimeImp::~CivilianTimeImp()
{
    //dtor
}

CivilianTimeImp::CivilianTimeImp(int hr, int min, int pm): TimeImp(hr, min){
    if(pm)
        strcpy(_whichM, " PM");
    else
        strcpy(_whichM, " AM");
}

void CivilianTimeImp::tell(){
    cout << "time is: " << _hr << ":" << _min << _whichM << endl;
}
