#include "ZuluTimeImp.h"

ZuluTimeImp::ZuluTimeImp()
{
    //ctor
}

ZuluTimeImp::~ZuluTimeImp()
{
    //dtor
}

ZuluTimeImp::ZuluTimeImp(int hr, int min, int zone): TimeImp(hr, min){
    if(zone == 5)
        strcpy(zone_, "Eastern Standard Time");
    else if(zone == 6)
        strcpy(zone_, "Central Standard Time");
}
void ZuluTimeImp::tell(){
    cout << "time is: " << _hr << ":" << _min << " " << zone_ << endl;
}
