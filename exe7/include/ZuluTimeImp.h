#ifndef ZULUTIMEIMP_H
#define ZULUTIMEIMP_H

#include<TimeImp.h>

class ZuluTimeImp: public TimeImp
{
    public:
        ZuluTimeImp();
        ZuluTimeImp(int hr, int min, int zone);
        void tell();
        virtual ~ZuluTimeImp();

    protected:
        char zone_[30];

    private:
};

#endif // ZULUTIMEIMP_H
