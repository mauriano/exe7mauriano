#ifndef CIVILIANTIME_H
#define CIVILIANTIME_H

#include<TimeA.h>
#include<CivilianTimeImp.h>


class CivilianTime: public TimeA
{
    public:
        CivilianTime();
        CivilianTime(int hr, int min, int pm);
        virtual ~CivilianTime();

    protected:

    private:
};

#endif // CIVILIANTIME_H
