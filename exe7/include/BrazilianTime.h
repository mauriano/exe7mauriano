#ifndef BRAZILIANTIME_H
#define BRAZILIANTIME_H

#include<TimeA.h>
#include<BrazilianTimeImp.h>

class BrazilianTime: public TimeA
{
    public:
        BrazilianTime();
        BrazilianTime(int hr, int min, int seg);
        virtual ~BrazilianTime();

    protected:

    private:
};

#endif // BRAZILIANTIME_H
