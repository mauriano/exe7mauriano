#ifndef CIVILIANTIMEIMP_H
#define CIVILIANTIMEIMP_H

#include<TimeImp.h>

class CivilianTimeImp: public TimeImp
{
    public:
        CivilianTimeImp();
        CivilianTimeImp(int hr, int min, int pm);
        void tell();
        virtual ~CivilianTimeImp();

    protected:
        char _whichM[4];

    private:
};

#endif // CIVILIANTIMEIMP_H
