#ifndef BRAZILIANTIMEIMP_H
#define BRAZILIANTIMEIMP_H


#include<TimeImp.h>

class BrazilianTimeImp: public TimeImp
{
    public:
        BrazilianTimeImp();
        BrazilianTimeImp(int hr, int min, int seg);
        void tell();
        virtual ~BrazilianTimeImp();

    protected:
        int segundos;

    private:
};

#endif // BRAZILIANTIMEIMP_H
