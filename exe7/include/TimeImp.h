#ifndef TIMEIMP_H
#define TIMEIMP_H

#include<iostream>
#include<string.h>
#include<iomanip>

using namespace std;

class TimeImp
{
    public:
        TimeImp();
        TimeImp(int hr, int min);
        virtual ~TimeImp();
        virtual void tell();

    protected:
        int _hr;
        int _min;

    private:
};

#endif // TIMEIMP_H
