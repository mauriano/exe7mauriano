#ifndef ZULUTIME_H
#define ZULUTIME_H

#include<TimeA.h>
#include<ZuluTimeImp.h>

class ZuluTime: public TimeA
{
    public:
        ZuluTime();
        ZuluTime(int hr, int min, int zone);
        virtual ~ZuluTime();

    protected:

    private:
};

#endif // ZULUTIME_H
