#ifndef TIMEA_H
#define TIMEA_H

#include<TimeImp.h>


class TimeA
{
    public:
        TimeA();
        TimeA(int hr, int min);
        virtual void tell();
        virtual ~TimeA();

    protected:
        TimeImp *imp_;

    private:
};

#endif // TIMEA_H
